# Statistics for Data Analysis

## Descriptive Statistics
    • Measures of central tendency
    • Measures of dispersion
    • Other measures - kurtosis and skewness...
    • Standard Statistical and Scientific Notation 

## Inferential Statistics
    • Sampling
    • Hypothesis formulation and testing.
    • Analysis of alpha and p-value s.
    • Parametric tests - e.g., Student's t-test, F test, ANOVA
    • Non-parametric tests -  e.g., chi-square test etc
    • The Central Limit Theorem
    • Confidence Intervals
    • Statistical power Probability
    • Probability Theory
    • Conditional and marginal 

## Probability
    • Bayes' Theorem
    • Common probability distributions
    • Expected value

## Regression
    • Correlation
    • Linear Regression
    • Multiple Linear Regression
    • Model selection and regularization methods (ridge and lasso)
    • Nonlinear models, splines and generalized additive models

## Classification
    • Näive Bayes Classifiers
    • Tree-based methods, random forests and boosting
    • Support Vector Machines

## Distributions
    • Normal
    • Hyper-Geometric
    • Negative binomial
    • Poisson
    • Logistic
    • Laplace
    • Rayleigh

## Hypothesis Tests/ Significance Level
    • Critical and acceptance regions
    • Confidence intervals
